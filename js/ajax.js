/**
 * WDF AJAX script
 * 
 * @package     The Web Development Framework
 * @version     0.1.3
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies
 * @lincese     Apache 2.0 http://www.apache.org/licenses/
 */

"use strict";

/**
 * POSTS data to a PHP script, and runs
 * a callback function (if supplied).
 */
function ajax (params)
{
    let ajx = new XMLHttpRequest();

    let url = params.url,
        data = params.data,
        success = params.success,
        error = params.error,
        query = obj2Post(data);
    
    if (typeof success !== "function") success = null;
    if (typeof error !== "function") error = null;

    ajx.open("POST", url, true);
    ajx.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajx.send(query);
    
    ajx.onreadystatechange = function ()
    {
        if (this.readyState == 4)
        {
            if (this.status == 200 && typeof success === 'function')
                success(this.responseText);
            else if (typeof error === 'function')
                error({'code': this.status, 'msg': this.statusText});
        }
    }
}