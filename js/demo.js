(function(){
    document.addEventListener("DOMContentLoaded", () =>
    {
        const frms = document.querySelectorAll('[data-form-submit="y"]');

        for (const frm of frms) // Listen for clicks on buttons found by query
        {
            frm.addEventListener("click", (e) =>
            {
                submit(e.target.form.id); // Call submit() with target form id
            });
        }
    });

    const submit = (fid) =>
    {
        const inputs = document.querySelectorAll(`#${fid} input`);
        let err = false, cnt = 0,
            values = [], fields = {};
        
        // Remove all errors on the submitted form
        html(`#${fid} label span.err`, '');

        // Get all values
        for (const input of inputs)
        {
            values[cnt] = input.value;
            if (values[cnt] === '')
            {
                append(
                    `#${fid} label[for="${input.name}"]`,
                    '<span class="err">This is a required field</span>'
                );
                err = true;
            }
            else
                fields[input.name] = input.value;

            cnt++;
        }

        if (err) return;

        ajax({
            url: "/ajax.php",
            data: fields,
            success: (result) => {
                html('#ajax_response', `<span class="msg">${result}</span>`);
            },
            error: (result) => {
                html('#ajax_response', `<span class="err">${result.msg}</span>`);
            }
        });
    }
})();