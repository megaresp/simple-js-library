/**
 * WDF JavaScript utility functions
 * 
 * @package     The Web Development Framework
 * @version     0.11.6
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies
 * @lincese     Apache 2.0 http://www.apache.org/licenses/
 */

"use strict";

const DEV = true;

/**
 * Writes an AJAX error to the console
 */
function ajaxError (data)
{
    out('AJAX request failed:');
    out(`${data.code}: ${data.msg}`);
    fatal('Terminating script');
}

/**
 * Appends markup to selector after existing content
 */
function append (selector, markup)
{
    const ele = document.querySelectorAll(selector);

    ele.forEach((item, index) => {
        item.innerHTML += markup;
    });
}

function deverror (msg)
{
    out(msg);
    return false;
}

/**
 * Throws an error which stops program execution, but
 * displays the content of msg.
 */
function fatal (msg)
{
    if (DEV)
        throw new Error(msg);
    else
        throw new Error('Unexpected condition');
}

/**
 * Returns the type of data
 */
function getType (data)
{
    let type = typeof data;
    
    if (type === 'number')
    {
        if (Number.isInteger(data)) type = 'integer';
        else type = 'float';
    }
    else if (type == 'object' && Array.isArray(data))
        type = 'array';

    return type;
}

/**
 * Replaces content of selector with markup
 */
function html (selector, markup)
{
    const ele = document.querySelectorAll(selector);
    
    ele.forEach((item, index) => {
        item.innerHTML = markup;
    });
}

/**
 * Return true if str contains JSON. False otherwise
 */
function isJson (str)
{
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

/**
 * Returns a POST query string build from
 * JavaScript object in data
 */
function obj2Post (data)
{
    out(data);
    out(typeof data);
    if (typeof data !== 'object') return;

    return Object.keys(data)
                 .map(key => `${key}=${data[key]}`)
                 .join('&');
}

/**
 * Console.logs data, giving type and name where name
 * is supplied. If data contains an object uses
 * console.dir instead.
 */
function out (data, name)
{
    if (DEV !== true) return; // We only output to console in DEVelopment mode (see top of this file)

    let type = null,
        output = '';

    if (typeof name !== "undefined")
    {
        type = getType(data);
        output = `${type} ${name}: `;
    }
    
    if (output !== '' && (type === 'array' || type === 'object'))
        console.log(output);
    
    if (typeof data === 'object')
        console.dir(data);
    else
        console.log(`${output}${data}`);
}

/**
 * Inserts markup before existing content in selector
 */
function prepend (selector, markup)
{
    const ele = document.querySelectorAll(selector);

    ele.forEach((item, index) => {
        item.innerHTML = markup + item.innerHTML;
    });
}

/**
 * Changes the value attribute of the selector
 * Works on things like input, option
 */
function val (selector, value)
{
    const ele = document.querySelectorAll(selector);

    ele.forEach((item, index) => {
        item.value = value;
    });
}