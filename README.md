# Simple JavaScript Library

An ultra-lightweight library of JavaScript and AJAX functions that help to build and debug a project. These tiny libraries can often be used to replace larger libraries where the JavaScript requirements are relatively modest.

The utility files are found in utils.js. The ajax() function is found in ajax.js and is dependent on utils.js. When using ajax.js, utils.js must also be present and loaded first. See the demonstration in index.html for more details.

The file demo.js demonstrates a call to the ajax() function. Both the demo.js and ajax.js functions make use of utility functions.

## Include in your project

Simply copy the files utils.js and ajax.js to a suitable folder in your project, and link to them in the usual way.

**Note**: The file index.html contains an example that links to these 2 files.

## AJAX

The ajax function works in a similar way to $.ajax() in jQuery. It is a POST only ajax caller, and does **not** work in browsers that don't support XMLHttpRequest.

See the file demo.js for a working example of how to call ajax() and handle the returned result.

This function requires 4 parameters as follows:
1. The URL of a script to POST the data to
2. A JavaScript object containing the POST data to send
3. A callback function to run on success
4. A callback function to run on failure

As you can see, it's very similar to $.ajax():
```javascript
ajax(
    'url': 'http://mysite.com/script.php',
    'data': object_of_key_value_pairs,
    'success': success_function(result),
    'error': fail_function(result)
);
```
## Utils

### function ajaxError (data)
Writes an error retured by ajax() to the console and terminates the script. Useful for handling ajax() errors.

### function append (selector, markup)
Appends whatever is in markup to the existing content of the element in the selector.

### function deverror (msg)
Allows a script to output the contents of msg to the console and return false using a single line. For example:

```javascript
// Without deverror
if (some_error)
{
    out("Some message");
    return false;
}

// With deverror
if (some_error)
    return deverror("Some message");
```

### function fatal (msg)
Throws an error which stops program execution, but displays the content of msg.

### function getType (data)
Returns the type of data, can distinguish between integers and floats, and arrays and objects. This function mimics the behaviour of PHP's gettype() function.

### function html (selector, markup)
Replaces content of selector with markup

### function isJson (str)
Return true if str contains JSON. False otherwise.

### function obj2Post (data)
Returns a POST query string built from JavaScript object in the parmeter data. Naturally, data must contain a set of key:value pairs for this to work.

### function out (data, name)
Send the value in data to console.log or console.dir as appropriate. It gives the type and name if the name parameter is supplied.

This function replicates that of the [PHP Utilities](https://gitlab.com/megaresp/php-html-library.git) version of out().

### function prepend (selector, markup)
Inserts markup before existing content in selector

### function val (selector, value)
Changes the value attribute of the selector. It's similar to html(), but used for form elements such as input and select.
