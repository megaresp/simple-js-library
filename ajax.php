<?php

/**
 * Demo ajax script
 * 
 * Provides a response to a POST query
 * 
 * @package     ajax.php
 * @version     0.1.2
 * @author      Wayne Davies
 * @copyright   Copyright (C) 2020, Wayne Davies
 * @lincese     Apache 2.0 http://www.apache.org/licenses/
 */

declare(strict_types=1);

foreach ($_POST as $key => $val)
    $$key = $val;

if (
    empty($security_key) ||
    $security_key !== '123' ||
    empty($firstname) ||
    empty($lastname)
)
{
    $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
    header($protocol . ' ' . 400);
    echo 'Bad request';
    exit;
}

echo 'You submitted the name ' . $firstname . ' ' . $lastname;